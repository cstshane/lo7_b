<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Playing with sessions</title>
    </head>
    <body>
<?php

if ( !isset( $_SESSION["counter"] ) )
{
    $_SESSION["counter"] = 0;
}

echo "<p>Counter is now " . ++$_SESSION["counter"] . "</p>\n";

?>
    </body>
</html>
