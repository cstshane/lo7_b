<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Test a cookie</title>
    </head>
    <body>
        <h1>Test a cookie</h1>
<?php

if ( isset( $_COOKIE["user"] ) )
{
    echo "<p>Welcome back, " . $_COOKIE["user"] . "</p>\n";
}
else
{
    echo "<p>You are not logged in.</p>\n";
}

?>
    </body>
</html>
