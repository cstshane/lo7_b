<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Learning Outcome 7 - Sessions and Cookies</title>
    </head>
    <body>
        <h1>Learning Outcome 7 - Sessions and Cookies</h1>
        <ol>
            <li><a href="7-Login.php">
                Log in to the website
                </a></li>
            <li><a href="7-CheckLogin.php">
                Check login status
                </a></li>
            <li><a href="7-Logout.php">
                Log out of the website
                </a></li>
        </ol>
    </body>
</html>
