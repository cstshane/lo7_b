<?php

require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );
$loader->addNamespaceMapping( "\\Formitron", "classes/Formitron" );

use CSTClasses_B\PasswordChecker;
use Formitron\BaseForm;
use Formitron\Element\Password;
use Formitron\Element\Text;
use Formitron\Helpers;

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Add a new user to the website</title>
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            <h1>Add a new user to the website</h1>
<?php

if ( !isset( $_POST["_submitted"] ) )
{
    // Create the new user form
    echo "<h2>Enter the information for the new user</h2>\n";
    $form = new BaseForm( BaseForm::METHOD_POST );
    $form->add( Helpers::withLabel( "username", "User Name",
            new Text( "username" ) ) );
    $form->add( Helpers::withLabel( "password", "Password",
            new Password( "password" ) ) );
    $form->add( Helpers::withLabel( "password2", "Re-enter password",
            new Password( "password2" ) ) );
    $form->add( Helpers::submitBlock( "login", "Add new user" ) );
    echo $form->render();

}
else
{
    // Verify that the entered passwords match
    if ( $_POST["password"] != $_POST["password2"] )
    {
        // Warn the user that the passwords don't match, and let him
        // try again
        echo "<p>The entered passwords do not match</p>";
        echo "<p><a href='#' onclick='history.back(); return false;'>Click";
        echo "    here</a> to try again</p>";
    }
    else
    {
        // Create an instance of the password checker
        $passwordChecker = new PasswordChecker();

        // Add the new user
        $user = $_POST["username"];
        $pwd = $_POST["password"];
        $success = $passwordChecker->addUser( $user, $pwd );

        if ( $success )
        {
            echo "<p>The user $user has been added</p>";
            echo "<p><a href='7-AddLogin.php'>Click here</a>\n";
            echo "    to add another</p>\n";
        }
        else
        {
            echo "<p>We failed to add $user to the website</p>";
            echo "<p><a href='#' onclick='history.back(); return false;'>Click";
            echo "    here</a> to try again</p>";
        }
    }
}

?>
        <div>
    </body>
</html>
