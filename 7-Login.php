<?php
include "7-SSLRedirect.php";

require_once __DIR__ . '/Autoloader.php';
$loader = new Autoloader();
$loader->addNamespaceMapping("\\CSTClasses_B",
        __DIR__ . '/../../private/CSTClasses_B' );
$loader->addNamespaceMapping( "\\Formitron", "classes/Formitron" );

use CSTClasses_B\PasswordChecker;
use Formitron\BaseForm;
use Formitron\Element\Password;
use Formitron\Element\Text;
use Formitron\Helpers;

// Start a session
session_start();

if ( isset( $_SESSION["loggedIn"] ) && $_SESSION["loggedIn"] == TRUE )
{
    header( "Location: 7-CheckLogin.php" );
    exit();
}

// If the form was submitted, log the visitor in
if ( isset( $_POST["_submitted"] ) )
{
    // Check to see if they are a valid user with a valid password
    $passwordChecker = new PasswordChecker();
    if ( $passwordChecker->isValid( $_POST["username"], $_POST["password"] ) )
    {
        $_SESSION["loggedIn"] = TRUE;
        session_regenerate_id( TRUE );
        
        // Note that we can't indicate login success or failure immediately --
        // we need to wait until we're outputting the webpage.
        // So, we'll set a flag to indicate if we failed the login.
        $loginSuccess = TRUE;
    }
    else
    {
        $loginSuccess = FALSE;
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Website Login Page</title>
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
<?php

if ( !isset( $_POST["_submitted"] ) )
{
    // Create the login form
    $form = new BaseForm( BaseForm::METHOD_POST );
    $form->add( Helpers::withLabel( "username", "User Name",
            new Text( "username" ) ) );
    $form->add( Helpers::withLabel( "password", "Password", 
            new Password( "password" ) ) );
    $form->add( Helpers::submitBlock( "login", "Login" ) );
    echo $form->render();
}
else if ( $loginSuccess )
{
?>
            <h1>You have been logged in</h1>
            <p><a href="7-Logout.php">Click here</a>
                to log out</p>
<?php
}
else
{
?>
            <p>You have entered an invalid username / password</p>
            <p><a href="7-Login.php">Click here</a>
                to try to login again</p>
<?php
}
?>
        </div>
    </body>
</html>
