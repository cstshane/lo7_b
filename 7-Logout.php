<?php

session_start();

// Set the loggedIn varaible to false
$_SESSION["loggedIn"] = FALSE;

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Website Logout Page</title>
    </head>
    <body>
        <p>You have been logged out</p>
        <p><a href="7-Login.php">Click here</a>
            to log in again</p>
    </body>
</html>
