<?php

session_start();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Website Check Login Page</title>
    </head>
    <body>
<?php

if ( isset( $_SESSION["loggedIn"] ) && $_SESSION["loggedIn"] == TRUE )
{
    echo "<p>You are logged in to the website</p>\n";
    echo "<p><a href='7-Logout.php'>Click here</a>\n";
    echo "to logout</p>\n";
}
else
{
    echo "<p>You are <strong>NOT</strong> logged in to the website</p>\n";
    echo "<p><a href='7-Login.php'>Click here</a>\n";
    echo "to go to the login page</p>\n";
}

?>
        
    </body>
</html>
